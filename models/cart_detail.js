"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class cart_detail extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			cart_detail.belongsTo(models.cart);
			cart_detail.belongsTo(models.product);
			// define association here
		}
	}
	cart_detail.init(
		{
			quantity: DataTypes.INTEGER,
			price: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: "cart_detail",
		}
	);
	return cart_detail;
};

// import module
const express = require("express");
const router = require("./router/index");

// inisialisasi server
const port = process.env.PORT || 3000;

// function
async function main() {
	try {
		const app = express();
		// middleware
		app.use(express.urlencoded({ extended: false }));
		app.use(express.json());
		app.use(router);

		// server
		app.listen(port, () => {
			console.log(`Example app listening at http://localhost:${port}`);
		});
	} catch (error) {
		console.log("main :", error);
	}
}

main();

const express = require("express");
const ProductController = require("../controllers/product.controller");
const router = express.Router();

router.get("/singleproduct/:id", ProductController.singleproduct);
router.get("/productbycategory/:id", ProductController.productbycategory);
router.get("/category", ProductController.category);
router.get("/all", ProductController.getall);

module.exports = router;

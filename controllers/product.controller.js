let productModel = require("../models").product;
let categoryModel = require("../models").category;

class ProductController {
	static async singleproduct(req, res) {
		try {
			const data = await productModel.findOne({
				include: [{ model: categoryModel }],
				where: { categoryId: req.params.id },
			});
			res.status(201).send(data);
		} catch (error) {
			res.send(error);
		}
	}
	static async productbycategory(req, res) {
		try {
			const data = await productModel.findAll({
				include: [{ model: categoryModel }],
				where: { categoryId: req.params.id },
			});
			res.status(201).send(data);
		} catch (error) {
			res.send(error);
		}
	}
	static async category(req, res) {
		try {
			const data = await categoryModel.findAll();
			res.status(201).send(data);
		} catch (error) {
			res.send(error);
		}
	}
	static async getall(req, res) {
		try {
			const data = await productModel.findAll({
				include: [{ model: categoryModel }],
			});
			res.status(201).send(data);
		} catch (error) {
			res.send(error);
		}
	}
}
module.exports = ProductController;
